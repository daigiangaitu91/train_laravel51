<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Tag extends Model
{
    protected $fillable = [
    'tag','title','subtitle','page_name','meta_description','reverse_direction'];

    public function posts()
    {
    	return $this->belongsToMany('App\Post','post_tag_pivot');
    }

    // public statis function addNeededTags(array $tags)
    // {
    // 	if (count($tags) === 0 ){
    // 		return;
    // 	}
    // 	$found = static::whereIn('tag',$tags)->lists('tag')->all();

    // 	foreach (array_diff($tags, $found) as $tag) {

    // 		static::create([
    // 			'tag' => $tag,
    // 			'title' => $tag,
    // 			'subtitle' => 'Subtitle for '.$tag,
    // 			'page_name' => '',
    // 			'meta_description' => '',
    // 			'reverse_direction' => false,
    // 			])
    // 	}
    // }

}
